-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema festivals
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema festivals
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `festivals` DEFAULT CHARACTER SET utf8 ;
USE `festivals` ;

-- -----------------------------------------------------
-- Table `festivals`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(200) NULL,
  `phone` VARCHAR(25) NULL,
  `email` VARCHAR(45) NULL,
  `last_login` DATETIME NULL,
  `is_blocked` TINYINT(1) NULL,
  `user_type` VARCHAR(20) NULL,
  `date_created` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`festival_name`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`festival_name` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `fb_link` VARCHAR(100) NULL,
  `twitter_link` VARCHAR(100) NULL,
  `instagram_link` VARCHAR(100) NULL,
  `youtube_link` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`festival`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`festival` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `place` VARCHAR(45) NULL,
  `date_from` DATETIME NULL,
  `date_to` DATETIME NULL,
  `festival_name` INT NOT NULL,
  `description` VARCHAR(2048) NULL,
  `number_of_visits` INT NOT NULL DEFAULT 0,
  `status` VARCHAR(20) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_festival_festival_name_idx` (`festival_name` ASC),
  CONSTRAINT `fk_festival_festival_name`
    FOREIGN KEY (`festival_name`)
    REFERENCES `festivals`.`festival_name` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`festival_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`festival_event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `performer_name` VARCHAR(45) NULL,
  `day` INT NULL,
  `date_from` DATETIME NULL,
  `date_to` DATETIME NULL,
  `festival_id` INT NOT NULL,
  `description` VARCHAR(2048) NULL,
  PRIMARY KEY (`id`, `festival_id`),
  INDEX `fk_festival_event_festival1_idx` (`festival_id` ASC),
  CONSTRAINT `fk_festival_event_festival1`
    FOREIGN KEY (`festival_id`)
    REFERENCES `festivals`.`festival` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`media`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`media` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `festival_id` INT NOT NULL,
  `is_video` TINYINT(1) NULL,
  `media_link` VARCHAR(100) NULL,
  `user_id` INT NOT NULL,
  `is_approved` TINYINT(1) NULL,
  PRIMARY KEY (`id`, `festival_id`, `user_id`),
  INDEX `fk_media_festival1_idx` (`festival_id` ASC),
  INDEX `fk_media_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_media_festival1`
    FOREIGN KEY (`festival_id`)
    REFERENCES `festivals`.`festival` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_media_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `festivals`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(256) NULL,
  `festival_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`, `festival_id`, `user_id`),
  INDEX `fk_comment_festival1_idx` (`festival_id` ASC),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_comment_festival1`
    FOREIGN KEY (`festival_id`)
    REFERENCES `festivals`.`festival` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `festivals`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`rating` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `festival_id` INT NOT NULL,
  `rating` INT NULL,
  PRIMARY KEY (`id`, `user_id`, `festival_id`),
  INDEX `fk_rating_user1_idx` (`user_id` ASC),
  INDEX `fk_rating_festival1_idx` (`festival_id` ASC),
  CONSTRAINT `fk_rating_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `festivals`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating_festival1`
    FOREIGN KEY (`festival_id`)
    REFERENCES `festivals`.`festival` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`message` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message` VARCHAR(1024) NULL,
  `date` DATETIME NULL,
  `festival_id` INT NOT NULL,
  PRIMARY KEY (`id`, `festival_id`),
  INDEX `fk_message_festival1_idx` (`festival_id` ASC),
  CONSTRAINT `fk_message_festival1`
    FOREIGN KEY (`festival_id`)
    REFERENCES `festivals`.`festival` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`ticket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `festival_id` INT NOT NULL,
  `price` DOUBLE NULL,
  `day` INT NULL DEFAULT 0,
  `type` VARCHAR(20) NULL,
  `available` INT NULL,
  `per_person` INT NULL,
  PRIMARY KEY (`id`, `festival_id`),
  INDEX `fk_ticket_festival1_idx` (`festival_id` ASC),
  CONSTRAINT `fk_ticket_festival1`
    FOREIGN KEY (`festival_id`)
    REFERENCES `festivals`.`festival` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `festivals`.`reservation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `festivals`.`reservation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `date_reserved` DATETIME NULL,
  `is_purchased` TINYINT(1) NULL,
  `ticket_id` INT NOT NULL,
  `festival_id` INT NOT NULL,
  `number_of_tickets` INT NULL,
  PRIMARY KEY (`id`, `user_id`, `ticket_id`, `festival_id`),
  INDEX `fk_resetvarion_user1_idx` (`user_id` ASC),
  INDEX `fk_reservation_ticket1_idx` (`ticket_id` ASC, `festival_id` ASC),
  CONSTRAINT `fk_resetvarion_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `festivals`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_ticket1`
    FOREIGN KEY (`ticket_id` , `festival_id`)
    REFERENCES `festivals`.`ticket` (`id` , `festival_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
