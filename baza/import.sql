INSERT INTO `festivals`.`user` (`id`, `firstname`, `lastname`, `username`, `password`, `phone`, `email`, `last_login`, `is_blocked`, `user_type`) VALUES ('1', 'Mihailo', 'Petric', 'admin', 'admin', '0693013870', 'pm130681d@student.etf.bg.ac.rs', 'null', '0', 'ADMIN');

INSERT INTO `festivals`.`user` (`id`, `firstname`, `lastname`, `username`, `password`, `phone`, `email`, `last_login`, `user_type`, `date_created`) VALUES ('2', 'Pera', 'Peric', 'pera', '123', '555-333', 'pera@pera.com', '', 'USER', '2017-02-15 19:05:53');

INSERT INTO `festivals`.`user` (`id`, `firstname`, `lastname`, `username`, `password`, `phone`, `email`, `last_login`, `user_type`, `date_created`) VALUES ('3', 'Test', 'Testic', 'test', '123', '555-333', 'test@test.com', '', 'USER', '2017-02-15 19:05:53');

INSERT INTO `festivals`.`festival_name` (`id`, `name`, `fb_link`, `twitter_link`, `instagram_link`, `youtube_link`) VALUES ('1', 'Exit', 'https://www.facebook.com/exit', 'http://twitter.com/exit', 'http://instagram.com/exitfest', 'https://youtube.com');

INSERT INTO `festivals`.`festival_name` (`id`, `name`, `fb_link`, `twitter_link`, `instagram_link`, `youtube_link`) VALUES ('2', 'Sun Dance', 'https://www.facebook.com/exit', 'http://twitter.com/exit', 'http://instagram.com/exitfest', 'https://youtube.com');

INSERT INTO `festivals`.`festival` (`id`, `place`, `date_from`, `date_to`, `festival_name`, `description`, `number_of_visits`, `status`) VALUES ('1', 'Novi Sad', '2017-02-16 00:00:00', '2017-02-17 00:00:00', '1', 'Exit festival opis', '0', 'ACTIVE');
INSERT INTO `festivals`.`festival` (`id`, `place`, `date_from`, `date_to`, `festival_name`, `description`, `number_of_visits`, `status`) VALUES ('2', 'Budva', '2017-03-16 00:00:00', '2017-03-18 00:00:00', '2', 'Na jazu', '0', 'ACTIVE');

INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('1', '1', '1000', '0', 'WHOLE_FESTIVAL', '20', '3');
INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('2', '1', '600', '1', 'DAILY', '200', '3');
INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('3', '1', '450', '2', 'DAILY', '200', '3');
INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('4', '2', '1500', '0', 'WHOLE_FESTIVAL', '50', '5');
INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('5', '2', '600', '1', 'DAILY', '300', '5');
INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('6', '2', '650', '2', 'DAILY', '700', '5');
INSERT INTO `festivals`.`ticket` (`id`, `festival_id`, `price`, `day`, `type`, `available`, `per_person`) VALUES ('7', '2', '700', '3', 'DAILY', '150', '5');

INSERT INTO `festivals`.`reservation` (`id`, `user_id`, `date_reserved`, `is_purchased`, `ticket_id`, `festival_id`, `number_of_tickets`) VALUES ('1', '2', '2017-02-14 00:00:00', '1', '1', '1', '2');

INSERT INTO `festivals`.`media` (`id`, `festival_id`, `is_video`, `media_link`, `user_id`, `is_approved`) VALUES ('1', '1', '0', '/media/pictures/1487510213920_test.png', '1', '1');

