/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.sun.istack.logging.Logger;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean(eager = true, name = "hibernateUtil")
@ApplicationScoped
public class HibernateUtil {

    private SessionFactory sessionFactory;

    @PostConstruct
    public void init() {
        try {
            Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
            StandardServiceRegistryBuilder sb = new StandardServiceRegistryBuilder();
            sb.applySettings(configuration.getProperties());
            StandardServiceRegistry sr = sb.build();
            sessionFactory = configuration.buildSessionFactory(sr);
            Logger.getLogger(HibernateUtil.class).log(Level.INFO, "HIBERNATE: session created");
        } catch (Throwable e) {
            System.err.println("HibernateUtil failed " + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    @PreDestroy
    public void close() {
        if (sessionFactory != null) {
            try {
                sessionFactory.close();
                Logger.getLogger(HibernateUtil.class).log(Level.INFO, "HIBERNATE: session closed");
            } catch (Throwable e) {
                Logger.getLogger(HibernateUtil.class).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
