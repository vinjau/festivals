/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.FestivalManagerBean;
import model.entity.Festival;
import model.entity.FestivalEvent;
import model.entity.FestivalName;
import model.entity.Ticket;
import model.entity.helpers.TicketType;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean(name = "csvParser")
@ApplicationScoped
public class CSVParser {

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public Festival parse(InputStream in) throws ParseException, IOException {
        Festival festival = null;
        BufferedReader br = null;

        br = new BufferedReader(new InputStreamReader(in));
        String line;
        if ((line = br.readLine()) != null) {
            festival = process(line, br);
        }

        return festival;
    }

    private Festival process(String line, BufferedReader br) throws IOException, ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        FestivalName festivalName = null;
        Festival festival = new Festival();
        String[] parts = line.split("\",\"");
        int festivalDays = 0;

        if (parts[0].substring(1).equals("Festival")) {
            String secondLine = br.readLine();
            String[] secondParts = secondLine.split("\",\"");
            String festivalNameString = secondParts[0].substring(1);
            festivalName = festivalManagerBean.getFestivalNameByName(festivalNameString);
            if (festivalName == null) {
                festivalName = new FestivalName();
                festivalName.setName(festivalNameString);
            }
            festival.setFestivalName(festivalName);

            String festivalPlaceString = secondParts[1];
            festival.setPlace(festivalPlaceString);

            String dateFromString = secondParts[2];
            Calendar dateFromCal = Calendar.getInstance();
            dateFromCal.setTime(dateFormat.parse(dateFromString));
            festival.setDateFrom(dateFromCal.getTime());

            String dateToString = secondParts[3].substring(0, secondParts[3].length() - 1);
            Calendar dateToCal = Calendar.getInstance();
            dateToCal.setTime(dateFormat.parse(dateToString));
            festival.setDateTo(dateToCal.getTime());

            long daysInMilis = festival.getDateTo().getTime() - festival.getDateFrom().getTime();
            daysInMilis /= 1000 * 60 * 60 * 24;
            festivalDays = (int) (1 + daysInMilis);
        }

        while ((line = br.readLine()) != null) {
            if (line.contains("TicketType")) {
                break;
            }
        }

        parts = line.split("\", \"");
        if (parts[0].substring(1).equals("TicketType")) {
            String secondLine = br.readLine();
            String[] secondParts = secondLine.split("\", \"");
            String thirdLine = br.readLine();
            String[] thirdParts = thirdLine.split("\", \"");

            List<Ticket> tickets = new ArrayList<>();
            if (secondParts[0].contains("per day")) {
                String priceString = secondParts[1].substring(0, secondParts[1].length() - 1);
                double price = Double.parseDouble(priceString);
                for (int i = 0; i < festivalDays; i++) {
                    Ticket ticket = new Ticket();
                    ticket.setDay(i + 1);
                    ticket.setAvailable(2000);
                    ticket.setFestival(festival);
                    ticket.setPerPerson(5);
                    ticket.setType(TicketType.DAILY);
                    ticket.setPrice(price);
                    tickets.add(ticket);
                }
            }

            if (thirdParts[0].contains("whole festival")) {
                String priceString = thirdParts[1].substring(0, thirdParts[1].length() - 1);
                double price = Double.parseDouble(priceString);
                Ticket ticket = new Ticket();
                ticket.setDay(0);
                ticket.setAvailable(2000);
                ticket.setFestival(festival);
                ticket.setPerPerson(5);
                ticket.setType(TicketType.WHOLE_FESTIVAL);
                ticket.setPrice(price);
                tickets.add(ticket);
            }
            festival.setTickets(tickets);
        }

        while ((line = br.readLine()) != null) {
            if (line.contains("Performer")) {
                break;
            }
        }

        parts = line.split("\",\"");
        if (parts[0].contains("Performer")) {
            Calendar today = Calendar.getInstance();
            today.setTime(festival.getDateFrom());
            Calendar nextDay = Calendar.getInstance();
            nextDay.setTime(festival.getDateFrom());
            nextDay.add(Calendar.DATE, 1);
            nextDay.add(Calendar.HOUR_OF_DAY, 9);
            int currentDay = 1;
            
            List<FestivalEvent> festivalEvents = new ArrayList<>();
            while ((line = br.readLine()) != null) {

                if (line.contains("Social Network")) {
                    break;
                }
                parts = line.split("\",\"");
                FestivalEvent festivalEvent = new FestivalEvent();
                festivalEvent.setFestival(festival);
                festivalEvent.setPerformerName(parts[0].substring(1, parts[0].length() - 1));
                
                String dateFromString = parts[1].trim();
                String dateToString = parts[2].trim();
                String timeFromString = parts[3];
                String timeToString = parts[4].substring(0, parts[4].length() - 1);
                String dateTimeFromString = dateFromString + " " + timeFromString;
                String dateTimeToString = dateToString + " " + timeToString;
                
                festivalEvent.setDateFrom(dateTimeFormat.parse(dateTimeFromString));
                festivalEvent.setDateTo(dateTimeFormat.parse(dateTimeToString));
                
                if (festivalEvent.getDateFrom().after(nextDay.getTime())) {
                    currentDay++;
                    nextDay.add(Calendar.DATE, 1);
                }
                
                festivalEvent.setDay(currentDay);
                festivalEvents.add(festivalEvent);
            }
            festival.setFestivalEvents(festivalEvents);
        }

        parts = line.split("\",\"");
        if (parts[0].contains("Social Network")) {
            while ((line = br.readLine()) != null) {
                parts = line.split("\",\"");
                if (parts[0].contains("Facebook")) {
                    festivalName.setFbLink(parts[1].substring(0, parts[1].length() - 1));
                }
                if (parts[0].contains("Twitter")) {
                    festivalName.setTwitterLink(parts[1].substring(0, parts[1].length() - 1));
                }
                if (parts[0].contains("Instagram")) {
                    festivalName.setInstagramLink(parts[1].substring(0, parts[1].length() - 1));
                }
                if (parts[0].contains("Youtube")) {
                    festivalName.setYoutubeLink(parts[1].substring(0, parts[1].length() - 1));
                }
            }

        }

        //save
        if (festivalName.getId() == 0) {
            festivalManagerBean.addNewFestivalName(festivalName);
        }
        festivalManagerBean.insertUploadedFestival(festival);
        return festival;
    }
}
