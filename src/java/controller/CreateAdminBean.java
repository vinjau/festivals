///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package controller;
//
//import java.util.Calendar;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ManagedProperty;
//import model.UserManagerBean;
//import model.entity.User;
//import model.entity.helpers.UserType;
//
///**
// *
// * @author TajnaSluzba
// */
//@ManagedBean(name = "createAdminBean")
//public class CreateAdminBean {
//   
//    
//    private String firstname;
//    private String lastname;
//    private String username;
//    private String password;
//    private String phone;
//    private String email;
//
//    private String message;
//
//    @ManagedProperty(value = "#{userManagerBean}")
//    private UserManagerBean userManagerBean;
//
//    public void setUserManagerBean(UserManagerBean userManagerBean) {
//        this.userManagerBean = userManagerBean;
//    }
//    
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    
//    
//    public void saveUser(){
//        
//        Calendar cal = Calendar.getInstance();
//       
//        
//        User user = new User();
//        user.setFirstname(firstname);
//        user.setLastname(lastname);
//        user.setUsername(username);
//        user.setPassword(password);
//        user.setPhone(phone);
//        user.setEmail(email);
//        user.setIsBloced(false);
//        user.setUserType(UserType.ADMIN);
//        user.setLastLogin(cal.getTime());
//      //  userDAO.insert(user);
//        
//        message = "TADA - SAVED";
//    }
//    
//    public void findUser() {
//        message = userManagerBean.getUser(username, password).getFirstname();
//    }
//    
//    public String getFirstname() {
//        return firstname;
//    }
//
//    public void setFirstname(String firstname) {
//        this.firstname = firstname;
//    }
//
//    public String getLastname() {
//        return lastname;
//    }
//
//    public void setLastname(String lastname) {
//        this.lastname = lastname;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getPhone() {
//        return phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//    
//    
//}
