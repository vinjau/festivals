/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.UserManagerBean;
import model.entity.User;
import model.entity.helpers.UserType;
import org.apache.commons.codec.digest.DigestUtils;
import util.FlashMessage;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class RegistrationController implements Serializable {

    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;

    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String phone;
    private String email;

    public void register() {
        boolean isErrorDetected = false;
        String pattern = "^(?=(.*[a-z]){3,})(?=(.*[A-Z]){1,})(?=(.*[0-9!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?]){1,})(?!.*(.)\\4\\4)[a-zA-z].{5,11}$";
        if (!password.matches(pattern)) {
            isErrorDetected = true;
        }
        if (userManagerBean.isUsernameTaken(username)) {
            FlashMessage.flash(FacesMessage.SEVERITY_ERROR, "Greška!", "Takvo korisničko ime već postoji!");
            isErrorDetected = true;
        }
        if (userManagerBean.isEmailTaken(email)) {
            FlashMessage.flash(FacesMessage.SEVERITY_ERROR, "Greška!", "Takav email već postoji!");
            isErrorDetected = true;
        }
        if (!isErrorDetected) {
            password = DigestUtils.sha1Hex(password);
            User user = new User();
            user.setFirstname(firstname);
            user.setLastname(lastname);
            user.setUsername(username);
            user.setPassword(password);
            user.setPhone(phone);
            user.setEmail(email);
            user.setLastLogin(null);
            user.setUserType(UserType.USER);
            user.setIsBloced(false);

            userManagerBean.insertUser(user);
        }
    }

    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
