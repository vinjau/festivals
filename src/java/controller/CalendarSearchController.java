/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseStream;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
public class CalendarSearchController {
    
    private Date calendar;

    public void onClick() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/addFestival.xhtml");
    }
    
    public Date getCalendar() {
        return calendar;
    }

    public void setCalendar(Date calendar) {
        this.calendar = calendar;
    }
}
