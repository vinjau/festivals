/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.helper.FestivalEventAdapter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.entity.Festival;
import model.entity.FestivalEvent;
import model.entity.Message;
import model.entity.helpers.UserType;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class FestivalEditController {

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    @ManagedProperty(value = "#{userController}")
    private UserController userController;

    private int festivalId;
    private Festival festival;
    private List<FestivalEventAdapter> festivalEventAdapters = new ArrayList<>();
    private Message message = new Message();
    private boolean canceled = false;
    private boolean showMessage = false;

    public void setup() {
        if (userController.getUser() != null && userController.getUser().getUserType().equals(UserType.ADMIN)) {
            festival = festivalManagerBean.getFestivalById(festivalId);
            message.setFestival(festival);
            for (FestivalEvent festivalEvent : festival.getFestivalEvents()) {
                FestivalEventAdapter fea = new FestivalEventAdapter();
                fea.setFestivalEvent(festivalEvent);
                festivalEventAdapters.add(fea);
            }
        }
    }

    public void cancelFestival() {
        canceled = true;
        showMessage = true;
    }

    public void addMessage() {
        showMessage = true;
    }

    public String confirm() {
        if (canceled) {
            festivalManagerBean.cancelFestival(festival);
        } else {
            List<FestivalEvent> newFestivalEvents = new ArrayList<>();
            for (FestivalEventAdapter fea : festivalEventAdapters) {
                newFestivalEvents.add(fea.getFestivalEvent());
            }
            festival.setFestivalEvents(newFestivalEvents);
            festivalManagerBean.updateFestival(festival);
        }
        festivalManagerBean.inserMessage(message);
        return "index?faces-redirect=true";
    }

    public void onEdit(FestivalEventAdapter fea) {
        for (FestivalEventAdapter f : festivalEventAdapters) {
            if (fea.equals(f)) {
                fea.setIsEditable(true);
            }
        }
    }

    public void onSave(FestivalEventAdapter fea) {
        for (FestivalEventAdapter f : festivalEventAdapters) {
            if (fea.equals(f)) {
                fea.setIsEditable(false);
            }
        }
    }
    
    public void addPerformer() {
        FestivalEventAdapter fea = new FestivalEventAdapter();
        fea.setIsEditable(true);
        festivalEventAdapters.add(fea);
    }

    public void onDelete(FestivalEventAdapter fea) {
        festivalEventAdapters.remove(fea);
    }

    public int getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(int festivalId) {
        this.festivalId = festivalId;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<FestivalEventAdapter> getFestivalEventAdapters() {
        return festivalEventAdapters;
    }

    public void setFestivalEventAdapters(List<FestivalEventAdapter> festivalEventAdapters) {
        this.festivalEventAdapters = festivalEventAdapters;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public boolean isShowMessage() {
        return showMessage;
    }

    public void setShowMessage(boolean showMessage) {
        this.showMessage = showMessage;
    }
}
