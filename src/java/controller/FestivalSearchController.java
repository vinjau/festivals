/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.entity.Festival;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class FestivalSearchController implements Serializable {
    
    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;
    
    @ManagedProperty(value = "#{festivalViewController}")
    private FestivalViewController festivalViewController;
    
    private List<Festival> festivals;
    private Festival selectedFestival;
    private int festivalId;
    private String festivalName;
    private String performer;
    private String place;
    private Date dateFrom;
    private Date dateTo;

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public void setFestivalViewController(FestivalViewController festivalViewController) {
        this.festivalViewController = festivalViewController;
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    public Festival getSelectedFestival() {
        return selectedFestival;
    }

    public void setSelectedFestival(Festival selectedFestival) {
        this.selectedFestival = selectedFestival;
    }
    
    @PostConstruct
    public void loadAllFestivals() {
        festivals = festivalManagerBean.getFestivals();
    }
    
    public String onRowSelect(SelectEvent selectEvent) {
        festivalViewController.setFestival(festivalManagerBean.getFestivalById(((Festival)selectEvent.getObject()).getId()));
        return "festival";
    }

    public int getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(int festivalId) {
        this.festivalId = festivalId;
    }
    
    public void search(){
        festivals = festivalManagerBean.search(festivalName, dateFrom, dateTo, place, performer);
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
}   
