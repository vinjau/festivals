/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.helper.FestivalEventAdapter;
import controller.helper.MediaAdapter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.MediaManagerBean;
import model.entity.Festival;
import model.entity.FestivalName;
import model.entity.FestivalEvent;
import model.entity.Media;
import model.entity.Ticket;
import model.entity.helpers.TicketType;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import util.FlashMessage;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class FestivalCreateController implements Serializable {

    private static final int BASIC_FESTIVAL_INFO = 1;
    private static final int EVENT_FESTIVAL_INFO = 2;
    private static final int TICKET_FESTIVAL_INFO = 3;
    private static final int MILIS_IN_ONE_HOUR = 1000 * 60 * 60 * 24;

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    @ManagedProperty(value = "#{userController}")
    private UserController userController;

    @ManagedProperty(value = "#{mediaManagerBean}")
    private MediaManagerBean mediaManagerBean;
    
    @ManagedProperty(value = "#{festivalViewController}")
    private FestivalViewController festivalViewController;

    private int stepActiveIndex = 0;
    private String festivalName;
    private int festivalId;
    private FestivalName newFestivalName = new FestivalName();
    private Festival newFestival = new Festival();
    private List<FestivalName> festivalNames;
    private int festivalDays;
    private Map<Integer, List<FestivalEventAdapter>> festivalEventAdapters = new HashMap<>();
    private List<MediaAdapter> newMediaAdapterList = new ArrayList<>();
    private Ticket allDaysTicket = new Ticket();
    private List<Ticket> tickets = new ArrayList<>();

    @PostConstruct
    public void initFestivalNames() {
        festivalNames = festivalManagerBean.getAllFestivalNames();
    }

    public void addNewFestivalName() {
        festivalManagerBean.addNewFestivalName(newFestivalName);
        festivalNames = festivalManagerBean.getAllFestivalNames();
        FlashMessage.flash(FacesMessage.SEVERITY_INFO, "INFO!", newFestivalName.getName() + " dodat u festivale");
    }

    public void addNewFestival() {
        newFestival.setFestivalName(festivalManagerBean.getFestivalNameByName(festivalName));
        festivalManagerBean.insertNewFestival(newFestival);
    }

    public void handleFileUpload(FileUploadEvent fileUploadEvent) {
        try {
            UploadedFile uploadedFile = fileUploadEvent.getFile();
            MediaAdapter mediaAdapter = new MediaAdapter();
            mediaAdapter.setFileName(uploadedFile.getFileName());
            mediaAdapter.setInputStream(uploadedFile.getInputstream());
            Media media = new Media();
            media.setIsApproved(true);
            if (uploadedFile.getFileName().endsWith("jpg") || uploadedFile.getFileName().endsWith("jpeg") || uploadedFile.getFileName().endsWith("png")) {
                media.setIsVideo(false);
            } else {
                media.setIsVideo(true);
            }
            mediaAdapter.setMedia(media);
            newMediaAdapterList.add(mediaAdapter);
        } catch (IOException ex) {
            Logger.getLogger(FestivalCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public List<FestivalName> getFestivalNames() {
        return festivalNames;
    }

    public void setFestivalNames(List<FestivalName> festivalNames) {
        this.festivalNames = festivalNames;
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public FestivalName getNewFestivalName() {
        return newFestivalName;
    }

    public void setNewFestivalName(FestivalName newFestivalName) {
        this.newFestivalName = newFestivalName;
    }

    public int getStepActiveIndex() {
        return stepActiveIndex;
    }

    public void setStepActiveIndex(int stepActiveIndex) {
        this.stepActiveIndex = stepActiveIndex;
    }

    public Festival getNewFestival() {
        return newFestival;
    }

    public void setNewFestival(Festival newFestival) {
        this.newFestival = newFestival;
    }

    public void back() {
        stepActiveIndex--;
    }

    public void next() {
        stepActiveIndex++;
        if (stepActiveIndex == BASIC_FESTIVAL_INFO) {
            FestivalName name = festivalManagerBean.getFestivalNameById(festivalId);
            festivalName = name.getName();
            newFestival.setFestivalName(name);
        }
        if (stepActiveIndex == EVENT_FESTIVAL_INFO) {
            festivalDays = (int) (newFestival.getDateTo().getTime() - newFestival.getDateFrom().getTime());
            festivalDays /= MILIS_IN_ONE_HOUR;
            festivalDays++;
            for (int i = 0; i < festivalDays; i++) {
                festivalEventAdapters.put(i, new ArrayList<>());
            }
        }
        if (stepActiveIndex == TICKET_FESTIVAL_INFO) {
            tickets.clear();
            allDaysTicket.setType(TicketType.WHOLE_FESTIVAL);
            allDaysTicket.setFestival(newFestival);
            for (int i = 0; i < festivalDays; i++) {
                Ticket ticket = new Ticket();
                ticket.setFestival(newFestival);
                ticket.setType(TicketType.DAILY);
                ticket.setDay(i + 1);
                tickets.add(ticket);
            }
        }
    }

    public String finish() {
        festivalManagerBean.insertNewFestival(newFestival);
        for (Integer dayNumber : festivalEventAdapters.keySet()) {
            for (FestivalEventAdapter fea : festivalEventAdapters.get(dayNumber)) {
                fea.getFestivalEvent().setFestival(newFestival);
                festivalManagerBean.addFestivalEvent(fea.getFestivalEvent());
            }
        }
        festivalManagerBean.addTicket(allDaysTicket);
        festivalManagerBean.addTickets(tickets);
        for (MediaAdapter mediaAdapter : newMediaAdapterList) {
            Media media = mediaAdapter.getMedia();
            media.setFestival(newFestival);
            media.setUser(userController.getUser());
            if (!media.isIsVideo()) {
                mediaManagerBean.insertPicture(media, mediaAdapter.getInputStream(), mediaAdapter.getFileName());
            } else {
                mediaManagerBean.insertVideo(media, mediaAdapter.getInputStream(), mediaAdapter.getFileName());
            }
        }
        festivalViewController.setFestival(newFestival);
        return "festival.xhtml?faces-redirect=true&id=" + newFestival.getId();
    }

    public void addNewFestivalEvent(Integer i) {
        List<FestivalEventAdapter> event = festivalEventAdapters.get(i);
        FestivalEventAdapter fea = new FestivalEventAdapter();
        FestivalEvent fe = new FestivalEvent();
        fe.setDay(i + 1);
        fea.setFestivalEvent(fe);
        fea.setIsEditable(true);
        event.add(fea);
    }

    public void onEdit(Integer dayIndex, Integer eventIndex) {
        List<FestivalEventAdapter> event = festivalEventAdapters.get(dayIndex);
        FestivalEventAdapter fea = event.get(eventIndex);
        fea.setIsEditable(true);
    }

    public void onSave(Integer dayIndex, Integer eventIndex) {
        List<FestivalEventAdapter> event = festivalEventAdapters.get(dayIndex);
        FestivalEventAdapter fea = event.get(eventIndex);
        fea.setIsEditable(false);
    }

    public void onDelete(Integer dayIndex, Integer eventIndex) {
        List<FestivalEventAdapter> event = festivalEventAdapters.get(dayIndex);
        event.remove((int) eventIndex);
    }

    public int getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(int festivalId) {
        this.festivalId = festivalId;
    }

    public int getFestivalDays() {
        return festivalDays;
    }

    public void setFestivalDays(int festivalDays) {
        this.festivalDays = festivalDays;
    }

    public Map<Integer, List<FestivalEventAdapter>> getFestivalEventAdapters() {
        return festivalEventAdapters;
    }

    public void setFestivalEventAdapters(Map<Integer, List<FestivalEventAdapter>> festivalEventAdapters) {
        this.festivalEventAdapters = festivalEventAdapters;
    }

    public void setMediaManagerBean(MediaManagerBean mediaManagerBean) {
        this.mediaManagerBean = mediaManagerBean;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public void setFestivalViewController(FestivalViewController festivalViewController) {
        this.festivalViewController = festivalViewController;
    }

    public Ticket getAllDaysTicket() {
        return allDaysTicket;
    }

    public void setAllDaysTicket(Ticket allDaysTicket) {
        this.allDaysTicket = allDaysTicket;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
