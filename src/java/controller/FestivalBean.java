/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.FestivalManagerBean;
import model.entity.Festival;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
public class FestivalBean {
    
    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;
    
    private List<Festival> festivals;
    
    public void festivalsOnClick() {
        festivals = festivalManagerBean.getFestivals();
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }
    
}
