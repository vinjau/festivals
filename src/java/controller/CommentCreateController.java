/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.helper.MediaAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.MediaManagerBean;
import model.UserManagerBean;
import model.entity.Comment;
import model.entity.Festival;
import model.entity.Media;
import model.entity.Rating;
import model.entity.User;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class CommentCreateController {

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;

    @ManagedProperty(value = "#{mediaManagerBean}")
    private MediaManagerBean mediaManagerBean;

    @ManagedProperty(value = "#{userController}")
    private UserController userController;

    private Festival festival;
    private int festivalId;
    private boolean commendable = false;
    private Comment comment = new Comment();
    private Rating rating = new Rating();
    private List<MediaAdapter> newMediaAdapterList = new ArrayList<>();

    public void setup() {
        if (userController.getUser() != null) {
            festival = festivalManagerBean.getFestivalById(festivalId);
            if (userManagerBean.isFestivalCommentable(festival, userController.getUser())) {
                commendable = true;
            }
        }
    }

    public void handleFileUpload(FileUploadEvent fileUploadEvent) {
        try {
            UploadedFile uploadedFile = fileUploadEvent.getFile();
            MediaAdapter mediaAdapter = new MediaAdapter();
            mediaAdapter.setFileName(uploadedFile.getFileName());
            mediaAdapter.setInputStream(uploadedFile.getInputstream());
            Media media = new Media();
            media.setIsApproved(false);
            media.setUser(userController.getUser());
            media.setFestival(festival);
            if (uploadedFile.getFileName().endsWith("jpg") || uploadedFile.getFileName().endsWith("jpeg") || uploadedFile.getFileName().endsWith("png")) {
                media.setIsVideo(false);
            } else {
                media.setIsVideo(true);
            }
            mediaAdapter.setMedia(media);
            newMediaAdapterList.add(mediaAdapter);
        } catch (IOException ex) {
            Logger.getLogger(FestivalCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String save() {
        User user = userController.getUser();
        comment.setFestival(festival);
        comment.setUser(user);
        rating.setFestival(festival);
        rating.setUser(user);
        userManagerBean.insertRating(rating);
        userManagerBean.insertComment(comment);
        for (MediaAdapter mediaAdapter : newMediaAdapterList) {
            Media media = mediaAdapter.getMedia();
            if (!media.isIsVideo()) {
                mediaManagerBean.insertPicture(media, mediaAdapter.getInputStream(), mediaAdapter.getFileName());
            } else {
                mediaManagerBean.insertVideo(media, mediaAdapter.getInputStream(), mediaAdapter.getFileName());
            }
        }
        return "index";
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public void setMediaManagerBean(MediaManagerBean mediaManagerBean) {
        this.mediaManagerBean = mediaManagerBean;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public int getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(int festivalId) {
        this.festivalId = festivalId;
    }

    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public boolean isCommendable() {
        return commendable;
    }

    public void setCommendable(boolean commendable) {
        this.commendable = commendable;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }
}
