/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.entity.Festival;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class UserViewController {
    
    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;
    
    private List<Festival> nextFiveFestivals;
    
    @PostConstruct
    public void init() {
        nextFiveFestivals = festivalManagerBean.getNextFiveFestivals();
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public List<Festival> getNextFiveFestivals() {
        return nextFiveFestivals;
    }

    public void setNextFiveFestivals(List<Festival> nextFiveFestivals) {
        this.nextFiveFestivals = nextFiveFestivals;
    }
}
