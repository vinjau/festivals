/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.FestivalManagerBean;
import model.UserManagerBean;
import model.entity.Festival;
import model.entity.Reservation;
import model.entity.Ticket;
import model.entity.helpers.UserType;
import util.FlashMessage;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class ReservationController implements Serializable {

    @ManagedProperty(value = "#{userController}")
    private UserController userController;

    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    private Festival festival;
    private int festivalId;
    private Reservation reservation = new Reservation();
    private int ticketsAvailableForPurchuse;
    private List<Integer> availableTickets = new ArrayList<>();
    private int ticketAmount;
    private boolean reservationFinished = false;
    private Ticket allDaysFestivalTicket;
    private boolean userBlockedFromReservation = false;

    public void loadFestival() {
        if (userController.getUser() != null && !userManagerBean.checkReservationBlock(userController.getUser())) {
            festival = festivalManagerBean.getFestivalById(festivalId);
            if (!userManagerBean.isBlockedFromFestival(userController.getUser(), festival)) {
                reservation.setFestival(festival);
                reservation.setTicket(festivalManagerBean.getFirstDayTicketByFestivalId(festivalId));
                reservation.setUser(userController.getUser());
                if (userController.getUser().getUserType().equals(UserType.USER)) {
                    reservation.setIsPurchased(false);
                } else {
                    reservation.setIsPurchased(true);
                }
                loadAvailableTickets();
            } else {
                userBlockedFromReservation = true;
                //return "festival?faces-redirect=true&amp;includeViewParams=true"; //DA SE URADI NESTO!!!!
            }
        }
        //return "";
    }

    public void loadAvailableTickets() {
        reservation.setTicket(festivalManagerBean.getTicketById(reservation.getTicket().getId()));
        if (reservation.getTicket() != null) {
            allDaysFestivalTicket = festivalManagerBean.getAllDaysTicketByFestivalId(festivalId);
            int ticketsReserved = userManagerBean.getReservationCountForTicketUser(userController.getUser(), reservation.getFestival());
            availableTickets.clear();
            ticketsAvailableForPurchuse = reservation.getTicket().getPerPerson() - ticketsReserved;
            if (ticketsAvailableForPurchuse > reservation.getTicket().getAvailable()) {
                ticketsAvailableForPurchuse = reservation.getTicket().getAvailable();
            }
            for (int i = 0; i < ticketsAvailableForPurchuse; i++) {
                availableTickets.add(i + 1);
            }
            if (ticketsAvailableForPurchuse > 0) {
                if (ticketAmount == 0) {
                    ticketAmount = 1;
                }
            } else {
                ticketAmount = 0;
            }
        }
    }

    public void makeReserevation() {
        if (ticketAmount > 0) {
            reservation.setNumberOfTickets(ticketAmount);
            if (!userManagerBean.addReservation(reservation)) {
                Ticket ticket = festivalManagerBean.getTicketById(reservation.getTicket().getId());
                if (ticket.getAvailable() == 0) {
                    FlashMessage.flash(FacesMessage.SEVERITY_WARN, "Greska!", "Nema više karata za izabrani dan");
                } else if (userController.getUser().getUserType().equals(UserType.USER)) {
                    FlashMessage.flash(FacesMessage.SEVERITY_WARN, "Greska!", "Nije moguće rezervisati tu količinu karata");
                } else {
                    FlashMessage.flash(FacesMessage.SEVERITY_WARN, "Greska!", "Nije moguće kupiti tu količinu karata");
                }
            } else {
                reservationFinished = true;
            }
        } else {
            FlashMessage.flash(FacesMessage.SEVERITY_WARN, "Greska!", "Niste u mogućnosti da rezervišete još karata ili nema više karata za izabrani dan");
        }
    }

    public boolean isUserBlockedFromReservation() {
        return userBlockedFromReservation;
    }

    public void setUserBlockedFromReservation(boolean userBlockedFromReservation) {
        this.userBlockedFromReservation = userBlockedFromReservation;
    }

    public int getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(int festivalId) {
        this.festivalId = festivalId;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public int getTicketsAvailableForPurchuse() {
        return ticketsAvailableForPurchuse;
    }

    public void setTicketsAvailableForPurchuse(int ticketsAvailableForPurchuse) {
        this.ticketsAvailableForPurchuse = ticketsAvailableForPurchuse;
    }

    public List<Integer> getAvailableTickets() {
        return availableTickets;
    }

    public void setAvailableTickets(List<Integer> availableTickets) {
        this.availableTickets = availableTickets;
    }

    public int getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(int ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public boolean isReservationFinished() {
        return reservationFinished;
    }

    public void setReservationFinished(boolean reservationFinished) {
        this.reservationFinished = reservationFinished;
    }

    public Ticket getAllDaysFestivalTicket() {
        return allDaysFestivalTicket;
    }

    public void setAllDaysFestivalTicket(Ticket allDaysFestivalTicket) {
        this.allDaysFestivalTicket = allDaysFestivalTicket;
    }
}
