/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.helper.WebPage;
import java.util.Calendar;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import model.UserManagerBean;
import model.entity.User;
import model.entity.helpers.UserType;
import org.apache.commons.codec.digest.DigestUtils;
import util.FlashMessage;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@RequestScoped
public class LoginController {

    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;

    @ManagedProperty(value = "#{userController}")
    private UserController userController;

    private String username;
    private String password;

    public String login() {
        User user = null;
        password = DigestUtils.sha1Hex(password);
        user = userManagerBean.getUser(username, password);
        if (user != null) {
            userController.setUser(user);
            updateLastLogin(user);
            if (UserType.ADMIN.equals(user.getUserType())) {
                return WebPage.ADMIN_START + "?faces-redirect=true";
            } else {
                return WebPage.USER_START + "?faces-redirect=true";
            }
        }
        FlashMessage.flash(FacesMessage.SEVERITY_FATAL, "Greška!", "Loši podaci.");
        return null;
    }

    public String logout() {
        userController.setUser(null);
        return WebPage.INDEX;
    }
    
    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private void updateLastLogin(User user) {
        Calendar calendar = Calendar.getInstance();
        user.setLastLogin(calendar.getTime());
        userManagerBean.updateUser(user);
    }
    
}
