/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.helper.ReservationAdapter;
import controller.helper.WebPage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.UserManagerBean;
import model.entity.Reservation;
import model.entity.Ticket;
import model.entity.User;
import model.entity.helpers.UserType;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class ReservationsViewController {

    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;

    @ManagedProperty(value = "#{userController}")
    private UserController userController;

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    private List<ReservationAdapter> reservationsAdapter = new ArrayList<>();
    private List<Reservation> reservations;
    private boolean cancelParam;
    private Date now = new Date();
    
    @PostConstruct
    public void init() {
        if (userController.getUser() != null) {
            List<Reservation> reservations = userManagerBean.getReservationsForUser(userController.getUser());
            for (Reservation reservation : reservations) {
                ReservationAdapter ra = new ReservationAdapter();
                ra.setReservation(reservation);
                reservationsAdapter.add(ra);
            }
        }
    }

    public void setup() {
        if (userController.getUser() != null) {
            if (cancelParam == true && userController.getUser().getUserType().equals(UserType.USER)) {
                reservations = userManagerBean.getCancelableReservations(userController.getUser());
                userManagerBean.getCommentedFestivalsForUser(userController.getUser());
            } else if (userController.getUser().getUserType().equals(UserType.ADMIN)) {
                reservations = userManagerBean.getAllReserved();
            } else if (cancelParam == false && userController.getUser().getUserType().equals(UserType.USER)) {
                reservations = userManagerBean.getPurchasedReservations(userController.getUser());
            }
        }
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public void cancelReservation(Reservation reservation) {
        Ticket ticket = reservation.getTicket();
        ticket.setAvailable(ticket.getAvailable() + reservation.getNumberOfTickets());
        festivalManagerBean.updateTicket(ticket);
        userManagerBean.cancelReservation(reservation);
        reservations.remove(reservation);
    }

    public void approveReservation(Reservation reservation) {
        userManagerBean.approveReservation(reservation);
        reservations.remove(reservation);
    }
    
    public boolean isFestivalFinished(Reservation reservation) {
        return reservation.getFestival().getDateTo().before(new Date());
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public boolean isCancelParam() {
        return cancelParam;
    }

    public void setCancelParam(boolean cancelParam) {
        this.cancelParam = cancelParam;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public List<ReservationAdapter> getReservationsAdapter() {
        return reservationsAdapter;
    }

    public void setReservationsAdapter(List<ReservationAdapter> reservationsAdapter) {
        this.reservationsAdapter = reservationsAdapter;
    }

    public Date getNow() {
        return now;
    }

    public void setNow(Date now) {
        this.now = now;
    }
}
