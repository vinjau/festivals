/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.entity.Festival;
import org.primefaces.event.FileUploadEvent;
import util.FlashMessage;
import util.parser.CSVParser;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class UploadFestivalController {
    
    @ManagedProperty(value = "#{csvParser}")
    private CSVParser csvParser;
    
    private Festival festival;
    private boolean nextEnabled;
    
    public void handleFileUpload(FileUploadEvent event) {
        try {
            festival = csvParser.parse(event.getFile().getInputstream());
        } catch (ParseException ex) {
            FlashMessage.flash(FacesMessage.SEVERITY_ERROR, "GRESKA!", "");
            Logger.getLogger(UploadFestivalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            FlashMessage.flash(FacesMessage.SEVERITY_ERROR, "GRESKA!", "");
            Logger.getLogger(UploadFestivalController.class.getName()).log(Level.SEVERE, null, ex);
        }
        FlashMessage.flash(FacesMessage.SEVERITY_INFO, "Gotovo!", "");
        nextEnabled = true;
    }

    public void setCsvParser(CSVParser csvParser) {
        this.csvParser = csvParser;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public boolean isNextEnabled() {
        return nextEnabled;
    }

    public void setNextEnabled(boolean nextEnabled) {
        this.nextEnabled = nextEnabled;
    }
}
