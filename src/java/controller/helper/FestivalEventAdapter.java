/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.helper;

import model.entity.FestivalEvent;

/**
 *
 * @author TajnaSluzba
 */
public class FestivalEventAdapter {
    
    private FestivalEvent festivalEvent;
    private boolean isEditable;

    public FestivalEvent getFestivalEvent() {
        return festivalEvent;
    }

    public void setFestivalEvent(FestivalEvent festivalEvent) {
        this.festivalEvent = festivalEvent;
    }

    public boolean isIsEditable() {
        return isEditable;
    }

    public void setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }
}
