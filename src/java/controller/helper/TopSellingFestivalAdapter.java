/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.helper;

import model.entity.FestivalName;

/**
 *
 * @author TajnaSluzba
 */
public class TopSellingFestivalAdapter {
    
    private long totalTickets;
    private int id;
    private FestivalName festivalName;

    public long getTotalTickets() {
        return totalTickets;
    }

    public void setTotalTickets(long totalTickets) {
        this.totalTickets = totalTickets;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FestivalName getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(FestivalName festivalName) {
        this.festivalName = festivalName;
    }
}
