/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.helper;

import java.util.Calendar;
import java.util.Date;
import model.entity.Reservation;

/**
 *
 * @author TajnaSluzba
 */
public class ReservationAdapter {
    
    private Reservation reservation;

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public boolean isCancelable() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);
        Date twoDays = cal.getTime();
        if (reservation.getDateReserved().after(twoDays) && !reservation.isIsPurchased()) {
            return true;
        }
        return false;
    }

    public boolean isCommentable() {
        Date today = new Date();
        if (reservation.isIsPurchased() && reservation.getFestival().getDateTo().before(today)) {
            return true;
        }
        return false;
    }
}
