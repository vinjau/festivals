/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.helper;

import model.entity.FestivalName;

/**
 *
 * @author TajnaSluzba
 */
public class TopRatedFestivalAdapter {

    private double averageRating;
    private int id;
    private FestivalName festivalName;

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FestivalName getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(FestivalName festivalName) {
        this.festivalName = festivalName;
    }
}
