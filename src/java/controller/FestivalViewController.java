/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.MediaManagerBean;
import model.UserManagerBean;
import model.entity.Festival;
import model.entity.FestivalEvent;
import model.entity.Media;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class FestivalViewController implements Serializable {

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;

    @ManagedProperty(value = "#{mediaManagerBean}")
    private MediaManagerBean mediaManagerBean;
    
    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;
    
    @ManagedProperty(value = "#{userController}")
    private UserController userController;
    
    private Festival festival;
    private int festivalId;
    private Map<Integer, List<FestivalEvent>> eventsPerDay = new HashMap<>();
    private Date[] datePerDay;
    private List<Media> mediaList = new ArrayList<>();
    private List<Media> pictures = new ArrayList<>();
    private List<Media> videos = new ArrayList<>();
    private boolean socialNetworksAvailable;
    private boolean[] socialNetwork = new boolean[4];
    private boolean commentable;
    private double avgRating;

    public void search() {
        festival = festivalManagerBean.getFestivalById(festivalId);
        avgRating = festivalManagerBean.getAverageRating(festival);
        festivalManagerBean.addVisitToFestival(festival);
        for (FestivalEvent festivalEvent : festival.getFestivalEvents()) {
            List<FestivalEvent> events = eventsPerDay.get(festivalEvent.getDay());
            if (events == null) {
                events = new ArrayList<>();
                eventsPerDay.put(festivalEvent.getDay(), events);
            }
            events.add(festivalEvent);
        }
        int festivalDays = eventsPerDay.keySet().size();
        Calendar firstFestivalDay = Calendar.getInstance();
        datePerDay = new Date[festivalDays];
        for (int i = 0; i < festivalDays; i++) {
            firstFestivalDay.setTime(festival.getDateFrom());
            firstFestivalDay.add(Calendar.DATE, i);
            datePerDay[i] = firstFestivalDay.getTime();
        }
        mediaList = mediaManagerBean.getAllMediaForFestival(festival);
        checkSocialNetworkAvailability();
        commentable = userManagerBean.isFestivalCommentable(festival, userController.getUser());
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public int getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(int festivalId) {
        this.festivalId = festivalId;
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public Map<Integer, List<FestivalEvent>> getEventsPerDay() {
        return eventsPerDay;
    }

    public void setEventsPerDay(Map<Integer, List<FestivalEvent>> eventsPerDay) {
        this.eventsPerDay = eventsPerDay;
    }

    public Date[] getDatePerDay() {
        return datePerDay;
    }

    public void setDatePerDay(Date[] datePerDay) {
        this.datePerDay = datePerDay;
    }

    public List<Media> getPictures() {
        return pictures;
    }

    public void setPictures(List<Media> pictures) {
        this.pictures = pictures;
    }

    public List<Media> getVideos() {
        return videos;
    }

    public void setVideos(List<Media> videos) {
        this.videos = videos;
    }

    public void setMediaManagerBean(MediaManagerBean mediaManagerBean) {
        this.mediaManagerBean = mediaManagerBean;
    }

    public List<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(List<Media> mediaList) {
        this.mediaList = mediaList;
    }

    public boolean isSocialNetworksAvailable() {
        return socialNetworksAvailable;
    }

    public void setSocialNetworksAvailable(boolean socialNetworksAvailable) {
        this.socialNetworksAvailable = socialNetworksAvailable;
    }

    private void checkSocialNetworkAvailability() {
        if (festival.getFestivalName().getFbLink() != null && !"".equals(festival.getFestivalName().getFbLink())){
            socialNetworksAvailable = true;
            socialNetwork[0] = true;
        }
        if (festival.getFestivalName().getTwitterLink()!= null && !"".equals(festival.getFestivalName().getTwitterLink())){
            socialNetworksAvailable = true;
            socialNetwork[1] = true;
        }
        if (festival.getFestivalName().getInstagramLink()!= null && !"".equals(festival.getFestivalName().getInstagramLink())){
            socialNetworksAvailable = true;
            socialNetwork[2] = true;
        }
        if (festival.getFestivalName().getYoutubeLink()!= null && !"".equals(festival.getFestivalName().getYoutubeLink())){
            socialNetworksAvailable = true;
            socialNetwork[3] = true;
        }
    }

    public boolean[] getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(boolean[] socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public boolean isCommentable() {
        return commentable;
    }

    public void setCommentable(boolean commentable) {
        this.commentable = commentable;
    }

    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }
}
