/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.helper.TopSellingFestivalAdapter;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.FestivalManagerBean;
import model.MediaManagerBean;
import model.UserManagerBean;
import model.entity.Festival;
import model.entity.User;
import util.FlashMessage;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ViewScoped
public class AdminViewController {

    @ManagedProperty(value = "#{festivalManagerBean}")
    private FestivalManagerBean festivalManagerBean;
    
    @ManagedProperty(value = "#{userManagerBean}")
    private UserManagerBean userManagerBean;
    
    @ManagedProperty(value = "#{mediaManagerBean}")
    private MediaManagerBean mediaManagerBean;
    
    private List<Festival> topFiveVisitedFestivals;
    private List<TopSellingFestivalAdapter> topFiveSellingFestivals;
    private boolean showLastTen = false;
    private List<User> lastTenRegistratedUsers;
    
    @PostConstruct
    public void init() {
        topFiveVisitedFestivals = festivalManagerBean.getTopFiveFestivals();
        topFiveSellingFestivals = festivalManagerBean.getTopSellingFestivals();
    }

    public void showLastTenUsers() {
        showLastTen = true;
        lastTenRegistratedUsers = userManagerBean.getLastTenRegistratedUsers();
    }
    
    public void approveAllNewMedia() {
        mediaManagerBean.approveAllNewMedia();
        FlashMessage.flash(FacesMessage.SEVERITY_INFO, "Odobreno!", "");
    }
    
    public List<Festival> getTopFiveVisitedFestivals() {
        return topFiveVisitedFestivals;
    }

    public void setTopFiveVisitedFestivals(List<Festival> topFiveVisitedFestivals) {
        this.topFiveVisitedFestivals = topFiveVisitedFestivals;
    }

    public void setFestivalManagerBean(FestivalManagerBean festivalManagerBean) {
        this.festivalManagerBean = festivalManagerBean;
    }

    public List<TopSellingFestivalAdapter> getTopFiveSellingFestivals() {
        return topFiveSellingFestivals;
    }

    public void setTopFiveSellingFestivals(List<TopSellingFestivalAdapter> topFiveSellingFestivals) {
        this.topFiveSellingFestivals = topFiveSellingFestivals;
    }

    public boolean isShowLastTen() {
        return showLastTen;
    }

    public void setShowLastTen(boolean showLastTenUsers) {
        this.showLastTen = showLastTenUsers;
    }

    public List<User> getLastTenRegistratedUsers() {
        return lastTenRegistratedUsers;
    }

    public void setLastTenRegistratedUsers(List<User> lastTenRegistratedUsers) {
        this.lastTenRegistratedUsers = lastTenRegistratedUsers;
    }

    public void setUserManagerBean(UserManagerBean userManagerBean) {
        this.userManagerBean = userManagerBean;
    }

    public void setMediaManagerBean(MediaManagerBean mediaManagerBean) {
        this.mediaManagerBean = mediaManagerBean;
    }
}
