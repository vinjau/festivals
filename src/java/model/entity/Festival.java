/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import model.entity.helpers.FestivalStatus;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author TajnaSluzba
 */
@Entity
public class Festival implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private String place;
    
    @Column(name = "date_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    
    @Column(name = "date_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "festival_name", nullable = false, updatable = false)
    private FestivalName festivalName;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "festival")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<FestivalEvent> festivalEvents;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "festival")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Ticket> tickets;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "festival")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Comment> comments;
    
    @OneToMany(mappedBy = "festival")
    private List<Reservation> reservations;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "festival")
    private List<Rating> ratings;
    
    private String description;
    
    @Enumerated(EnumType.STRING)
    private FestivalStatus status;
    
    @Column(name = "number_of_visits")
    private int numberOfVisits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public FestivalName getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(FestivalName festivalName) {
        this.festivalName = festivalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfVisits() {
        return numberOfVisits;
    }

    public void setNumberOfVisits(int numberOfVisits) {
        this.numberOfVisits = numberOfVisits;
    }

    public List<FestivalEvent> getFestivalEvents() {
        return festivalEvents;
    }

    public void setFestivalEvents(List<FestivalEvent> festivalEvents) {
        this.festivalEvents = festivalEvents;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addVisit() {
        numberOfVisits++;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public FestivalStatus getStatus() {
        return status;
    }

    public void setStatus(FestivalStatus status) {
        this.status = status;
    }
}
