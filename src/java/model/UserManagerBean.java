/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.dao.CommentDAO;
import model.dao.RatingDAO;
import model.dao.ReservationDAO;
import model.dao.UserDAO;
import model.entity.Comment;
import model.entity.Festival;
import model.entity.Rating;
import model.entity.Reservation;
import model.entity.User;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class UserManagerBean implements Serializable {

    @ManagedProperty(value = "#{userDAO}")
    private UserDAO userDao;

    @ManagedProperty(value = "#{reservationDAO}")
    private ReservationDAO reservationDAO;

    @ManagedProperty(value = "#{commentDAO}")
    private CommentDAO commentDAO;

    @ManagedProperty(value = "#{ratingDAO}")
    private RatingDAO ratingDAO;

    public void setRatingDAO(RatingDAO ratingDAO) {
        this.ratingDAO = ratingDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    public void setUserDao(UserDAO userDao) {
        this.userDao = userDao;
    }

    public void setReservationDAO(ReservationDAO reservationDAO) {
        this.reservationDAO = reservationDAO;
    }

    public void insertUser(User user) {
        userDao.insert(user);
    }

    public void updateUser(User user) {
        userDao.update(user);
    }

    public User getUser(String username, String password) {
        return userDao.getUser(username, password);
    }

    public boolean isUsernameTaken(String username) {
        return userDao.getUserByUserName(username) != null;
    }

    public boolean isEmailTaken(String email) {
        return userDao.getUserByEmail(email) != null;
    }

    public boolean addReservation(Reservation reservation) {
        return reservationDAO.insert(reservation);
    }

    public int getReservationCountForTicketUser(User user, Festival festival) {
        return reservationDAO.getReservationCountForTicketUser(user, festival);
    }

    public List<Reservation> getReservationsForUser(User user) {
        return reservationDAO.getReservationsForUser(user);
    }

    public List<Reservation> getCancelableReservations(User user) {
        return reservationDAO.getCancelableReservations(user);
    }

    public void cancelReservation(Reservation reservation) {
        reservationDAO.deleteReservation(reservation);
    }

    public boolean checkReservationBlock(User user) {
        return reservationDAO.checkReservationBlock(user);
    }

    public boolean isBlockedFromFestival(User user, Festival festival) {
        return reservationDAO.isUserBlockedFromFestival(user, festival);
    }

    public List<Festival> getCommentedFestivalsForUser(User user) {
        return commentDAO.getCommentedFestivalsForUser(user);
    }

    public void approveReservation(Reservation reservation) {
        reservation.setIsPurchased(true);
        reservationDAO.update(reservation);
    }

    public List<Reservation> getAllReserved() {
        return reservationDAO.getAllReserved();
    }

    public List<User> getLastTenRegistratedUsers() {
        return userDao.getLastTenRegistrated();
    }

    public List<Reservation> getPurchasedReservations(User user) {
        return reservationDAO.getPurchasedReservations(user);
    }

    public boolean isFestivalCommentable(Festival festival, User user) {
        return reservationDAO.isFestivalCommentable(festival, user);
    }

    public void insertRating(Rating rating) {
        ratingDAO.insert(rating);
    }

    public void insertComment(Comment comment) {
        commentDAO.insert(comment);
    }
}
