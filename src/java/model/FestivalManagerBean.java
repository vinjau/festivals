/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.helper.TopRatedFestivalAdapter;
import controller.helper.TopSellingFestivalAdapter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.dao.CommentDAO;
import model.dao.FestivalDAO;
import model.dao.FestivalEventDAO;
import model.dao.FestivalNameDAO;
import model.dao.MessageDAO;
import model.dao.RatingDAO;
import model.dao.TicketDAO;
import model.entity.Festival;
import model.entity.FestivalEvent;
import model.entity.FestivalName;
import model.entity.Message;
import model.entity.Ticket;
import model.entity.helpers.FestivalStatus;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class FestivalManagerBean implements Serializable {

    @ManagedProperty(value = "#{festivalDAO}")
    private FestivalDAO festivalDAO;

    @ManagedProperty(value = "#{festivalNameDAO}")
    private FestivalNameDAO festivalNameDAO;

    @ManagedProperty(value = "#{festivalEventDAO}")
    private FestivalEventDAO festivalEventDAO;

    @ManagedProperty(value = "#{ticketDAO}")
    private TicketDAO ticketDAO;

    @ManagedProperty(value = "#{commentDAO}")
    private CommentDAO commentDAO;

    @ManagedProperty(value = "#{ratingDAO}")
    private RatingDAO ratingDAO;

    @ManagedProperty(value = "#{messageDAO}")
    private MessageDAO messageDAO;

    public void setMessageDAO(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }
    
    public void setRatingDAO(RatingDAO ratingDAO) {
        this.ratingDAO = ratingDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    public void insertNewFestival(Festival festival) {
        festivalDAO.insert(festival);
    }
    
    public void insertUploadedFestival(Festival festival) {
        festivalDAO.insertUploaded(festival);
    }

    public List<Festival> getFestivals() {
        return festivalDAO.getFestivals();
    }

    public List<FestivalName> getAllFestivalNames() {
        return festivalNameDAO.getAllFestivalNames();
    }

    public void setFestivalDAO(FestivalDAO festivalDAO) {
        this.festivalDAO = festivalDAO;
    }

    public void setFestivalNameDAO(FestivalNameDAO festivalNameDAO) {
        this.festivalNameDAO = festivalNameDAO;
    }

    public void addNewFestivalName(FestivalName festivalName) {
        festivalNameDAO.insert(festivalName);
    }

    public FestivalName getFestivalNameByName(String name) {
        return festivalNameDAO.getFestivalByName(name);
    }

    public FestivalName getFestivalNameById(int id) {
        return festivalNameDAO.getFestivalById(id);
    }

    public void setFestivalEventDAO(FestivalEventDAO festivalEventDAO) {
        this.festivalEventDAO = festivalEventDAO;
    }

    public void addFestivalEvent(FestivalEvent festivalEvent) {
        festivalEventDAO.insert(festivalEvent);
    }

    public Festival getFestivalById(int id) {
        return festivalDAO.getFestivalById(id);
    }

    public void addTickets(List<Ticket> tickets) {
        ticketDAO.insert(tickets);
    }

    public void setTicketDAO(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
    }

    public void addTicket(Ticket ticket) {
        ticketDAO.insert(ticket);
    }

    public List<Festival> search(String name, Date dateFrom, Date dateTo, String place, String performer) {
        return festivalDAO.search(name, dateFrom, dateTo, place, performer);
    }

    public Ticket getTicketById(int id) {
        return ticketDAO.getTicketsById(id);
    }

    public Ticket getFirstDayTicketByFestivalId(int id) {
        return ticketDAO.getFirstDayTicketsByFestivalId(id);
    }

    public Ticket getAllDaysTicketByFestivalId(int id) {
        return ticketDAO.getWholeFestivalTicketByFestivalId(id);
    }

    public void addVisitToFestival(Festival festival) {
        festivalDAO.addVisitToFestival(festival);
    }

    public List<Festival> getNextFiveFestivals() {
        return festivalDAO.nextFiveFestivals();
    }

    public void updateTicket(Ticket ticket) {
        ticketDAO.update(ticket);
    }

    public List<Festival> getTopFiveFestivals() {
        return festivalDAO.getTopFiveFestivals();
    }

    public List<TopSellingFestivalAdapter> getTopSellingFestivals() {
        List<TopSellingFestivalAdapter> festivalAdapter = new ArrayList<>();
        for (Object o : festivalDAO.getTopSellingTicketsFestivals()) {
            TopSellingFestivalAdapter adapter = new TopSellingFestivalAdapter();
            Object[] oo = (Object[]) o;
            adapter.setTotalTickets((long) oo[0]);
            adapter.setId((int) oo[1]);
            adapter.setFestivalName((FestivalName) oo[2]);
            festivalAdapter.add(adapter);
        }
        return festivalAdapter;
    }
    
    public double getAverageRating(Festival festival) {
        return ratingDAO.getAverageRating(festival);
    }
    
    public List<TopRatedFestivalAdapter> getTopFiveRatedFestivals() {
        List<TopRatedFestivalAdapter> festivalAdapter = new ArrayList<>();
        for (Object o : festivalDAO.getTopSellingTicketsFestivals()) {
            TopRatedFestivalAdapter adapter = new TopRatedFestivalAdapter();
            Object[] oo = (Object[]) o;
            adapter.setAverageRating((double) oo[0]);
            adapter.setId((int) oo[1]);
            adapter.setFestivalName((FestivalName) oo[2]);
            festivalAdapter.add(adapter);
        }
        return festivalAdapter;
    }
    
    public void updateFestival(Festival festival) {
        festivalDAO.update(festival);
    }
    
    public void cancelFestival(Festival festival) {
        festival.setStatus(FestivalStatus.CANCELED);
        festivalDAO.update(festival);
    }
    
    public void inserMessage(Message message) {
        messageDAO.insert(message);
    }
}
