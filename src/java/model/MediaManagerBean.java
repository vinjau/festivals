/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.dao.MediaDAO;
import model.entity.Festival;
import model.entity.Media;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class MediaManagerBean {

    public static final String PICTURE_SAVE_PATH = "C:\\Temp\\festivals\\media\\pictures\\";
    public static final String VIDEO_SAVE_PATH = "C:\\Temp\\festivals\\media\\videos\\";
    public static final String PICTURE_MEDIA_LINK = "/media/pictures/";
    public static final String VIDEO_MEDIA_LINK = "media/videos/";

    @ManagedProperty(value = "#{mediaDAO}")
    private MediaDAO mediaDAO;

    public void setMediaDAO(MediaDAO mediaDAO) {
        this.mediaDAO = mediaDAO;
    }

    public void insertPicture(Media media, InputStream in, String fileName) {
        media.setIsVideo(false);
        File file = new File(PICTURE_SAVE_PATH + System.currentTimeMillis() + "_" + fileName );
        Path path = file.toPath();
        try {
            Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
            media.setMediaLink(PICTURE_MEDIA_LINK + file.getName());
            mediaDAO.insert(media);
        } catch (IOException ex) {
            Logger.getLogger(MediaManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertVideo(Media media, InputStream in, String fileName) {
        media.setIsVideo(true);
        File file = new File(VIDEO_SAVE_PATH + System.currentTimeMillis() + "_" + fileName );
        Path path = file.toPath();
        try {
            Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
            media.setMediaLink(VIDEO_MEDIA_LINK + file.getName());
            mediaDAO.insert(media);
        } catch (IOException ex) {
            Logger.getLogger(MediaManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Media> getAllMediaForFestival(Festival festival) {
        return mediaDAO.getAllMediaForFestival(festival);
    }
    
        
    public void approveAllNewMedia() {
        mediaDAO.approveAll();
    }
}
