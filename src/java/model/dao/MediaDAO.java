/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.entity.Festival;
import model.entity.Media;
import model.entity.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import util.HibernateUtil;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class MediaDAO {

    @ManagedProperty(value = "#{hibernateUtil}")
    private HibernateUtil hibernateUtil;

    public void setHibernateUtil(HibernateUtil hibernateUtil) {
        this.hibernateUtil = hibernateUtil;
    }

    public void insert(Media media) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(media);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Media> getAllMediaForFestival(Festival festival) {
        List<Media> mediaList = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Media.class);
            mediaList = query.add(Restrictions.and(Restrictions.eq("festival", festival), Restrictions.eq("isApproved", true))).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return mediaList;
    }

    public void approveAll() {
        List<Media> mediaList = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Media.class);
            mediaList = query.add(Restrictions.eq("isApproved", false)).list();
            for (Media media : mediaList) {
                media.setIsApproved(true);
                session.update(media);
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
