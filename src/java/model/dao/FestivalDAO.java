/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.entity.Festival;
import model.entity.helpers.FestivalStatus;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import util.HibernateUtil;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class FestivalDAO {

    private static final int FIVE_RESULTS = 5;

    @ManagedProperty(value = "#{hibernateUtil}")
    private HibernateUtil hibernateUtil;

    public void insert(Festival festival) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            festival.setStatus(FestivalStatus.ACTIVE);
            session.save(festival);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Festival> getFestivals() {
        List<Festival> festivals = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            festivals = query.list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return festivals;
    }

    public Festival getFestivalById(int id) {
        Festival festival = null;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            festival = (Festival) query.add(Restrictions.eq("id", id)).uniqueResult();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return festival;
    }

    public List<Festival> search(String festivalName, Date dateFrom, Date dateTo, String place, String performer) {
        List<Festival> festivals = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            List<Criterion> predicates = new ArrayList<>();
            if (dateFrom != null) {
                predicates.add(Restrictions.ge("dateFrom", dateFrom));
            }
            if (dateTo != null) {
                predicates.add(Restrictions.le("dateTo", dateTo));
            }
            if (place != null && !"".equals(place)) {
                predicates.add(Restrictions.like("place", "%" + place + "%"));
            }
            if (performer != null && !"".equals(performer)) {
                query.createAlias("festivalEvents", "fe", JoinType.INNER_JOIN, Restrictions.like("fe.performerName", "%" + performer + "%"));
            }
            if (festivalName != null && !"".equals(festivalName)) {
                query.createAlias("festivalName", "fn", JoinType.INNER_JOIN, Restrictions.like("fn.name", "%" + festivalName + "%"));
            }
            Conjunction conjunction = new Conjunction();
            for (Criterion c : predicates) {
                conjunction.add(c);
            }
            Date today = new Date();
            conjunction.add(Restrictions.gt("dateTo", today));
            query.add(conjunction);
            festivals = query.list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return festivals;
    }

    public void addVisitToFestival(Festival festival) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            festival.addVisit();
            session.update(festival);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Festival> nextFiveFestivals() {
        List<Festival> festivals = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            Date today = new Date();
            festivals = query.add(Restrictions.and(Restrictions.gt("dateTo", today))).addOrder(Order.asc("dateFrom")).setMaxResults(FIVE_RESULTS).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return festivals;
    }

    public void setHibernateUtil(HibernateUtil hibernateUtil) {
        this.hibernateUtil = hibernateUtil;
    }

    public List<Festival> getTopFiveFestivals() {
        List<Festival> festivals = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            Date today = new Date();
            festivals = query.addOrder(Order.desc("numberOfVisits")).setMaxResults(FIVE_RESULTS).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return festivals;
    }

    public List getTopSellingTicketsFestivals() {
        List result = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            query.createAlias("reservations", "r", JoinType.INNER_JOIN);
            result = query.setProjection(Projections.projectionList().add(
                    Projections.sum("r.numberOfTickets").as("sum")).add(Projections.groupProperty("id")).add(Projections.groupProperty("festivalName"))
            ).addOrder(Order.desc("sum")).setMaxResults(FIVE_RESULTS).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }
    
    public void update(Festival festival) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(festival);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
    
    public void delete(Festival festival) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(festival);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List getTopFiveRatedFestivals() {
        List result = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            query.createAlias("ratings", "r", JoinType.INNER_JOIN);
            result = query.setProjection(Projections.projectionList().add(
                    Projections.avg("r.rating").as("avg")).add(Projections.groupProperty("id")).add(Projections.groupProperty("festivalName"))
            ).addOrder(Order.desc("avg")).setMaxResults(FIVE_RESULTS).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }
    
    

    public void insertUploaded(Festival festival) {
                Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            festival.setStatus(FestivalStatus.CANCELED);
            session.save(festival);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
