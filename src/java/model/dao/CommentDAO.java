/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.entity.Comment;
import model.entity.Festival;
import model.entity.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import util.HibernateUtil;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class CommentDAO {

    @ManagedProperty(value = "#{hibernateUtil}")
    private HibernateUtil hibernateUtil;

    public void insert(Comment comment) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(comment);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Comment> getCommentsForFestival(Festival festival) {
        List<Comment> comments = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Comment.class);
            comments = query.add(Restrictions.eq("festival", festival)).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return comments;
    }

    public List<Festival> getCommentedFestivalsForUser(User user) {
        List<Festival> festivals = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -2);
            Date twoDays = cal.getTime();
            Criteria query = session.createCriteria(Festival.class);
            festivals = query.createAlias("comments", "c", JoinType.INNER_JOIN)
                    .createAlias("reservations", "r", JoinType.INNER_JOIN)
                    .add(Restrictions.and(Restrictions.eq("r.isPurchased", true), Restrictions.lt("r.dateReserved", twoDays))).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return festivals;
    }

    public void setHibernateUtil(HibernateUtil hibernateUtil) {
        this.hibernateUtil = hibernateUtil;
    }
}
