/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import model.entity.Comment;
import model.entity.Festival;
import model.entity.Reservation;
import model.entity.Ticket;
import model.entity.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import util.HibernateUtil;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class ReservationDAO {

    @ManagedProperty(value = "#{hibernateUtil}")
    private HibernateUtil hibernateUtil;

    public boolean insert(Reservation reservation) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        boolean successfulInsert = false;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Ticket.class);
            Ticket ticket = (Ticket) query.add(Restrictions.eq("id", reservation.getTicket().getId())).uniqueResult();
            if (ticket.getAvailable() - reservation.getNumberOfTickets() >= 0) {
                ticket.setAvailable(ticket.getAvailable() - reservation.getNumberOfTickets());
                session.update(ticket);
                reservation.addTimeStamp();
                session.save(reservation);
                successfulInsert = true;
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return successfulInsert;
    }

    public List<Reservation> getNewReservations(User user) {
        List<Reservation> reservations = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            reservations = query.add(Restrictions.eq("isPurchased", 0)).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reservations;
    }

    public int getReservationCountForTicketUser(User user, Festival festival) {
        int count = 0;
        List<Reservation> reservations;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            reservations = query.add(Restrictions.and(Restrictions.eq("user", user), Restrictions.eq("festival", festival))).list();
            for (Reservation reservation : reservations) {
                count += reservation.getNumberOfTickets();
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return count;
    }

    public List<Reservation> getReservationsForUser(User user) {
        List<Reservation> reservations = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            reservations = query.add(Restrictions.eq("user", user)).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reservations;
    }

    public boolean checkReservationBlock(User user) {
        boolean userBlocked = false;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -2);
            Date twoDays = cal.getTime();
            long count = (long) query.add(Restrictions.and(Restrictions.eq("user", user), Restrictions.eq("isPurchased", false), Restrictions.lt("dateReserved", twoDays)))
                    .setProjection(Projections.count("id")).uniqueResult();
            if (count >= 3) {
                user.setIsBloced(true);
                session.update(user);
                userBlocked = true;
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return userBlocked;
    }

    public List<Reservation> getCancelableReservations(User user) {
        List<Reservation> reservations = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -2);
            Date twoDays = cal.getTime();
            reservations = query.add(Restrictions.and(Restrictions.eq("user", user), Restrictions.eq("isPurchased", false), Restrictions.ge("dateReserved", twoDays))).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reservations;
    }

    public void deleteReservation(Reservation reservation) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(reservation);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void setHibernateUtil(HibernateUtil hibernateUtil) {
        this.hibernateUtil = hibernateUtil;
    }

    public boolean isUserBlockedFromFestival(User user, Festival festival) {
        boolean userBlocked = false;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -2);
            Date twoDays = cal.getTime();
            long count = (long) query.add(Restrictions.and(Restrictions.eq("user", user), Restrictions.eq("festival", festival), Restrictions.eq("isPurchased", false), Restrictions.lt("dateReserved", twoDays)))
                    .setProjection(Projections.count("id")).uniqueResult();
            if (count >= 1) {
                userBlocked = true;
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return userBlocked;
    }

    public List<Reservation> getAllReserved() {
        List<Reservation> reservations = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            reservations = query.add(Restrictions.eq("isPurchased", false)).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reservations;
    }

    public void update(Reservation reservation) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(reservation);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Reservation> getPurchasedReservations(User user) {
        List<Reservation> reservations = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Reservation.class);
            reservations = query.add(Restrictions.and(Restrictions.eq("isPurchased", true), Restrictions.eq("user", user))).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reservations;
    }

    public boolean isFestivalCommentable(Festival festival, User user) {
        boolean commentable = false;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Festival.class);
            query.createAlias("reservations", "r", JoinType.INNER_JOIN);
            List result = query.add(Restrictions.and(Restrictions.lt("dateTo", new Date()), Restrictions.eq("r.isPurchased", true), Restrictions.eq("r.user", user))).list();
            int count = result.size();
            if (count > 0) {
                Criteria query2 = session.createCriteria(Comment.class);
                query2.createAlias("festival", "f", JoinType.INNER_JOIN);
                query2.createAlias("user", "u", JoinType.INNER_JOIN);
                query2.add(Restrictions.and(Restrictions.eq("f.id", festival.getId()), Restrictions.eq("u.id", user.getId())));
                List result2 = query2.list();
                int count2 = result2.size();
                if (count2 == 0) {
                    commentable = true;
                }
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return commentable;
    }
}
