/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import util.HibernateUtil;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import model.entity.Ticket;
import model.entity.helpers.TicketType;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

/**
 *
 * @author TajnaSluzba
 */
@ManagedBean
@ApplicationScoped
public class TicketDAO {

    @ManagedProperty(value = "#{hibernateUtil}")
    private HibernateUtil hibernateUtil;

    public void setHibernateUtil(HibernateUtil hibernateUtil) {
        this.hibernateUtil = hibernateUtil;
    }

    public void insert(Ticket ticket) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(ticket);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void insert(List<Ticket> tickets) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            for (Ticket ticket : tickets) {
                session.save(ticket);
            }
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Ticket> getTicketsByFestivalId(int id) {
        List<Ticket> tickets = new ArrayList<>();
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Ticket.class);
            query.createAlias("festival", "f", JoinType.INNER_JOIN);
            tickets = query.add(Restrictions.and(Restrictions.eq("f.id", id), Restrictions.eq("type", TicketType.DAILY))).list();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return tickets;
    }
    
    public Ticket getWholeFestivalTicketByFestivalId(int id) {
        Ticket ticket = null;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Ticket.class);
            query.createAlias("festival", "f", JoinType.INNER_JOIN);
            ticket = (Ticket) query.add(Restrictions.and(Restrictions.eq("f.id", id), Restrictions.eq("type", TicketType.WHOLE_FESTIVAL))).uniqueResult();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return ticket;
    }
    
    public Ticket getTicketsById(int id) {
        Ticket ticket = null;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Ticket.class);
            ticket = (Ticket) query.add(Restrictions.eq("id", id)).uniqueResult();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return ticket;
    }
    
    public Ticket getFirstDayTicketsByFestivalId(int id) {
        Ticket ticket = null;
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria query = session.createCriteria(Ticket.class);
            query.createAlias("festival", "f", JoinType.INNER_JOIN);
            ticket = (Ticket) query.add(Restrictions.and(Restrictions.eq("f.id", id), Restrictions.eq("day", 1))).uniqueResult();
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return ticket;
    }
    
    public void update(Ticket ticket) {
        Session session = hibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(ticket);
            tx.commit();
            session.flush();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
